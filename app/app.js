(function () {
    'use strict';

    angular
        .module('myApp', [
            'ngMaterial',
            'ui.sortable',
            'LocalStorageModule'
        ]);

}());
