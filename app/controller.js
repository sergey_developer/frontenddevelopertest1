(function () {
    'use strict';

    angular
        .module('myApp')
        .controller('testCtrl', AppController);

    AppController.$inject = ['$mdDialog', 'localStorageService'];

    /* @ngInject */
    function AppController($mdDialog, localStorageService) {
        /* jshint validthis: true */
        var vm = this;
        vm.getItems = getItems;
        vm.addItem = addItem;
        vm.editItem = editItem;
        vm.delItem = delItem;
        vm.delItems = delItems;

        var defaultItems = [
            { name: 'First item', checked: false },
            { name: 'Second item', checked: false },
            { name: 'Third item', checked: false },
            { name: 'Fourth item', checked: false }
        ];
        var updateLocalItems = function() {
            localStorageService.set('items', vm.items);
        };

        vm.items = [];
        vm.sortableOptions = {
            update: function(event, ui) {
                setTimeout(function() {
                    updateLocalItems();
                }, 10);
            }
        };

        function getItems() {
            var items = localStorageService.get('items');
            if (items) {
                this.items = items;
            } else {
                this.items = defaultItems;
            }
        }

        function addItem(ev) {
            var confirm = $mdDialog.prompt()
                .title('Add new item!')
                .textContent('Type item name.')
                .placeholder('item name')
                .ariaLabel('Item name')
                .targetEvent(ev)
                .ok('Save')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function(result) {
                if( result ){
                    vm.items.push({ name: result, checked: false });
                    updateLocalItems();
                }
            });
        }

        function editItem(index, ev) {
            var confirm = $mdDialog.prompt()
                .title('Edit item!')
                .textContent('Type item name.')
                .placeholder('item name')
                .initialValue(this.items[index].name)
                .ariaLabel('Item name')
                .targetEvent(ev)
                .ok('Save')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function(result) {
                if( result ){
                    vm.items[index].name = result;
                    updateLocalItems();
                }
            });
        }

        function delItem(index, ev) {
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete this item?')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Please do it!')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                vm.items.splice(index, 1);
                updateLocalItems();
            });
        }

        function delItems(ev) {
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete checked items?')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Please do it!')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                vm.items = vm.items.filter(function(item) {
                    return item.checked !== true;
                });
                updateLocalItems();
            });
        }
    }
}());