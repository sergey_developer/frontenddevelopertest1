(function () {
    'use strict';

    angular
        .module('myApp')
        .config(AppConfig);

    AppConfig.$inject = ['$mdIconProvider', 'localStorageServiceProvider'];

    /* @ngInject */
    function AppConfig($mdIconProvider, localStorageServiceProvider) {
        $mdIconProvider.iconSet('trash', '../img/trash-icon.png', 24);
        localStorageServiceProvider.setPrefix('myApp');
    }

}());